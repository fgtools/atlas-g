/* ==========================================================
    *** DO NOT MODIFY config.h ***
    Instead modify config.cmake.h, and re-run cmake configure
   ========================================================== */
#ifndef _CONFIG_H_
#define _CONFIG_H_

#ifdef _MSC_VER
/* ==================================================================== */
/* Windows/MSVC things */
#ifndef FGBASE_DIR
#define FGBASE_DIR "X:/fgdata"
#endif
/* --------------------------------------------------------------
 * Option: USE_GLEW_LIB
 * Use GLEW 1.7.0, or higher, to add GL extensions
 * was just an experiment with GLEW library - in Map.cxx - 
 * BUT now in repo base thus can NOT be turned OFF
 * -------------------------------------------------------------- */
#define USE_GLEW_LIB
#define HAVE_GLEXT_H
#undef  EXCLUDE_GLEXT_H
#define HAVE_SGGLEXT_H

/* -------------------------------------------------------------- */

/* --------------------------------------------------------------
 * Option: HAVE_TRI_UNORDERED
 * if you are in a windows system which HAS unordered_set
 * and unordered_map, as part of the std::tr1 implementation, then
 * you can try defining HAVE_TRI_UNORDERED                        */
#undef HAVE_TRI_UNORDERED
/* -------------------------------------------------------------- */

/* some simple 'defines' to get unix functions */
#define strcasecmp stricmp
#define strncasecmp strnicmp
#define va_copy(a,b)    a = b
#define log2(a)     ((log((double)(a)) / log(2.0)) )
#define log2f(a)    (float)log2(a)
//#define round(d) (int)(d + 0.5) // conflict with SGMisc::round<double>()!!!
#define rint(x) ((x > 0.0) ? floor( x + 0.5 ) : (x < 0.0) ? ceil( x - 0.5 ) : 0.0)
#define lrintf(x) rint(x)
#define asprintf atlas_asprintf

// was #if (defined(ATLAS_MAP) && defined(USE_GLEW_153)), but now in both
#ifdef USE_GLEW_LIB
/* ------------------------------------------------------------
 * WAS only for Map.cxx - ie ATLAS_MAP defined -
 * Use glext.h, from http://www.opengl.org/registry/api/glext.h,
 * functions by GLEW 1.7.0 from http://glew.sourceforge.net/
 * ------------------------------------------------------------ */
#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif
#ifndef HAVE_GLEXT_H
#define HAVE_GLEXT_H
#endif
#ifdef EXCLUDE_GLEXT_H
#undef EXCLUDE_GLEXT_H
#endif
#include <GL/glew.h>    // get GL extensions

/* Use the GLEW static library */
#ifdef ADD_LIB_PRAGMA
# if defined(WIN64)
#  ifdef NDEBUG
#   pragma comment (lib, "glew64s.lib")   /* glew-1.5.3 static lib - Release  */
#  else
#   pragma comment (lib, "glew64sd.lib")   /* glew-1.5.3 static lib - Debug  */
#  endif
# else /* !WIN64 */
#ifdef NDEBUG
#pragma comment (lib, "glew32s.lib")   /* glew-1.5.3 static lib - Release  */
#else
#pragma comment (lib, "glew32sd.lib")   /* glew-1.5.3 static lib - Debug  */
#endif
# endif /* WIN64 y/n */
#endif /*  #ifdef ADD_LIB_PRAGMA */

#endif  /* #if defined(USE_GLEW_LIB)) */

/* some replacement functions  */
#include "strsep.h"
#include "asprintf.h"
#include "win_utils.h"
#include <io.h>
#include <direct.h>

#ifndef VERSION
#define VERSION  "0.4.9-MSVC10-WIN32"
#endif

/* ==================================================================== */
#else /* !_MSC_VER */
/* ==================================================================== */

#ifndef FGBASE_DIR
/* what is a suitable default? */
#define FGBASE_DIR "../fgdata"
#endif

/* ==================================================================== */
#endif /* _MSC_VER y/n */

// For config.h.cmake file
/* Define to 1 if you have the `ceil' function. */
#cmakedefine HAVE_CEIL 1

/* Define to 1 if you have the `fabs' function. */
#cmakedefine HAVE_FABS 1

/* Define to 1 if you have the <fcntl.h> header file. */
#cmakedefine HAVE_FCNTL_H 1

/* Define to 1 if you have the `floor' function. */
#cmakedefine HAVE_FLOOR 1

/* Define to 1 if you have the `fmod' function. */
#cmakedefine HAVE_FMOD 1

/* Define to 1 if you have the <inttypes.h> header file. */
#cmakedefine HAVE_INTTYPES_H 1

/* Define to 1 if you have the <memory.h> header file. */
#cmakedefine HAVE_MEMORY_H 1

/* Defined if the optreset variable exists. */
#cmakedefine HAVE_OPTRESET 1

/* Define to 1 if you have the `rint' function. */
#cmakedefine HAVE_RINT 1

/* Define to 1 if you have the `sqrt' function. */
#cmakedefine HAVE_SQRT 1

/* Define to 1 if you have the <stdint.h> header file. */
#cmakedefine HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#ifndef HAVE_STDLIB_H
#cmakedefine HAVE_STDLIB_H 1
#endif

/* Define to 1 if you have the `strcspn' function. */
#cmakedefine HAVE_STRCSPN 1

/* Define to 1 if you have the <strings.h> header file. */
#cmakedefine HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#cmakedefine HAVE_STRING_H 1

/* Define to 1 if you have the `strspn' function. */
#cmakedefine HAVE_STRSPN 1

/* Define to 1 if you have the `strstr' function. */
#cmakedefine HAVE_STRSTR 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#cmakedefine HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#cmakedefine HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the `tolower' function. */
#cmakedefine HAVE_TOLOWER 1

/* Define to 1 if you have the <unistd.h> header file. */
#cmakedefine HAVE_UNISTD_H 1

/* Define to 1 if you have the <values.h> header file. */
#cmakedefine HAVE_VALUES_H 1

/* Define to 1 if you have the `vprintf' function. */
#cmakedefine HAVE_VPRINTF 1

/* Define to 1 if you have the `vsnprintf' function. */
#cmakedefine HAVE_VSNPRINTF 1

/* Define to 1 if you have the <winbase.h> header file. */
#cmakedefine HAVE_WINBASE_H 1

/* Define to 1 if you have the <windows.h> header file. */
#cmakedefine HAVE_WINDOWS_H 1

/* Define to the address where bug reports for this package should be sent. */
#ifndef PACKAGE_BUGREPORT
#define PACKAGE_BUGREPORT "reports _AT_ geoffair _DOT_ info"
#endif

/* Define to the full name of this package. */
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "Atlasg"
#endif

/* Define to the full name and version of this package. */
#ifndef PACKAGE_STRING
#define PACKAGE_STRING "Atlasg 0.4.11"
#endif

/* Define to the one symbol short name of this package. */
#ifndef PACKAGE_TARNAME
#define PACKAGE_TARNAME "atlasg"
#endif

/* Define to the home page for this package. */
#ifndef PACKAGE_URL
#define PACKAGE_URL "http://atlas.sourceforge.net/"
#endif

/* Define to the version of this package. */
#ifndef PACKAGE_VERSION
#define PACKAGE_VERSION "0.4.11"
#endif

/* Define to 1 if you have the ANSI C header files. 
   Well who doesn't? And anyway not used in SOURCE */
#define STDC_HEADERS 1

/* Define to 1 if you have the `curl' library (-lcurl). */
#cmakedefine HAVE_LIBCURL 1

/* Define to 1 if you have the `GLEW' library (-lGLEW). */
#cmakedefine HAVE_LIBGLEW 1

/* Define to 1 if you have the `plibul' library (-lplibul). */
#cmakedefine HAVE_LIBPLIBUL 1

/* Some items from the config.h.msvc */

/* -------------------------------------------------------------
   Option to LIMIT mapping to a 10x10 chunk, and an option to
   force overwrite of existing maps
 * ------------------------------------------------------------- */
#define ADD_CHUNK_LIMIT

/* The following failed in this script */
#if 0 // 000000000000000000000000000000000000000
/* Define for fxmesa */
#undef FX
/* Define to 1 if you don't have `vprintf' but do have `_doprnt.' */
#undef HAVE_DOPRNT
/* Define to 1 if you have the `m' library (-lm). */
#undef HAVE_LIBM
/* Define to 1 if you have the `pthread' library (-lpthread). */
#undef HAVE_LIBPTHREAD
/* Define as the return type of signal handlers (`int' or `void'). */
#undef RETSIGTYPE
/* Define to 1 if you can safely include both <sys/time.h> and <time.h>. */
#undef TIME_WITH_SYS_TIME
/* Define to 1 if your <sys/time.h> declares `struct tm'. */
#undef TM_IN_SYS_TIME
/* Define for fxmesa */
#undef XMESA
#endif // 000000000000000000000000000000000000000

/* Original generated 2014/08/12 17:39:16 UTC, by in2cmake.pl */
#endif // _CONFIG_H_
/* eof */
